# s3p - Push files for static website to AWS S3 bucket

Pushes files for static website to AWS S3 bucket. Optionally empties bucket
before push. Requires YAML formatted configuration providing mapping of file
extensions to content types (key: 'content_map') and (optionally) filename
patterns to be excluded from push (key: 'excl_patterns').

Directory `site_files` has a simple example. For more info run

    python s3p.py -h
