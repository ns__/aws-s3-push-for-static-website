#! /usr/bin/env python3

"""
Pushes files for static website to AWS S3 bucket. Optionally empties bucket
before push. Requires YAML formatted configuration providing mapping of file
extensions to content types (key: 'content_map') and (optionally) filename
patterns to be excluded from push (key: 'excl_patterns').
"""

import os
import re
from fnmatch import fnmatch
import argparse
import logging
import yaml
import boto3


def site_files(top_dir='.', incl=None, excl=None):
    """
    Yields all files under `top_dir`, complete with path relative to top_dir.
    Includes only files that match any of the patterns in `incl` (sequence of
    str) and skips any file that matches  any of the patterns provided in `excl`
    (sequence of str).
    """
    def name_matches(name, patterns):
        for pat in patterns:
            if fnmatch(name, pat):
                return True
        return False

    incl = incl if incl else ['*',]
    excl = excl if excl else []
    for path, _, files in os.walk(top_dir):
        for f in files:
            file_path = os.path.join(path, f)
            full = re.sub(r'^{}/'.format(top_dir), '', file_path)
            if name_matches(full, incl) and not name_matches(full, excl):
                yield full


def read_conf(conf_file):
    with open(conf_file) as fp:
        conf = yaml.load(fp, Loader=yaml.FullLoader)
    return conf


def main(top_dir, bucket_name, profile, content_map, incl_patterns,
        excl_patterns, clear=False):
    session = boto3.Session(profile_name=profile)
    bucket = session.resource('s3').Bucket(bucket_name)
    if clear:
        for obj in bucket.objects.all():
            logging.info('deleting object %s', obj.key)
            obj.delete()
    for key in site_files(top_dir, incl_patterns, excl_patterns):
        _, ext = os.path.splitext(key)
        content_type = content_map.get(ext, None)
        extra = {'ContentType': content_type} if content_type else {}
        src = os.path.join(top_dir, key)
        logging.info('uploading %s as %s with content type %s', src, key,
                content_type)
        bucket.upload_file(src, key, ExtraArgs=extra)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('top', help="""top level directory""")
    parser.add_argument('bucket', help="""name of S3 bucket to push to""")
    parser.add_argument('--profile', default='default', help="""AWS access
            credential profile name to use; defaults to 'default' (see boto3
            docs for details)""")
    parser.add_argument('--clear', action='store_true', help="""if provided S3
            bucket will be emptied before pushing files""")
    parser.add_argument('--only', default='*', help="""pattern to limit files
            being incuded in push (matched against full file path, defaults to
            '*')""")
    script, _ = os.path.splitext(__file__)
    parser.add_argument('--conf', default=script +
            '.yaml', help="""YAML-formatted config file; default:
            {}""".format(script + '.yaml'))
    parser.add_argument('--log_level', default='INFO', help="""specify log
            level(default: 'INFO'; other options: 'DEBUG', 'WARNING', 'ERROR',
            'CRITICAL')""")

    args = parser.parse_args()

    logging.basicConfig(level=getattr(logging, args.log_level))
    conf = read_conf(args.conf)

    main(args.top, args.bucket, args.profile, conf['content_map'],
            [args.only], conf.get('excl_patterns', None), args.clear)
